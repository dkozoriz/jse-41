package ru.t1.dkozoriz.tm.command.system;

import ru.t1.dkozoriz.tm.dto.request.system.ServerAboutRequest;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public ApplicationAboutCommand() {
        super("about", "show developer info.", "-a");
    }

    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: " + getEndpointLocator().getSystemEndpoint().getAbout(new ServerAboutRequest()).getName());
        System.out.println("E-mail: " + getEndpointLocator().getSystemEndpoint().getAbout(new ServerAboutRequest()).getEmail() + "\n");

        System.out.println("[APPLICATION]");
        System.out.println("NAME: " + getEndpointLocator().getSystemEndpoint().getAbout(new ServerAboutRequest()).getApplicationName() + "\n");

        System.out.println("[GIT]");
        System.out.println("BRANCH: " + getEndpointLocator().getSystemEndpoint().getAbout(new ServerAboutRequest()).getBranch());
        System.out.println("COMMIT ID: " + getEndpointLocator().getSystemEndpoint().getAbout(new ServerAboutRequest()).getCommitId());
        System.out.println("COMMITTER NAME: " + getEndpointLocator().getSystemEndpoint().getAbout(new ServerAboutRequest()).getCommitterName());
        System.out.println("COMMITTER EMAIL: " + getEndpointLocator().getSystemEndpoint().getAbout(new ServerAboutRequest()).getCommitterEmail());
        System.out.println("MESSAGE: " + getEndpointLocator().getSystemEndpoint().getAbout(new ServerAboutRequest()).getMessage());
        System.out.println("TIME: " + getEndpointLocator().getSystemEndpoint().getAbout(new ServerAboutRequest()).getTime());
    }

}