package ru.t1.dkozoriz.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.dkozoriz.tm.dto.model.UserDTO;

public final class UserViewProfileCommand extends AbstractUserCommand {

    public UserViewProfileCommand() {
        super("view-user-profile", "view profile of current user.");
    }

    @Override
    public void execute() {
        System.out.println("[USER PROFILE]");
        @NotNull final UserDTO user = getEndpointLocator().getAuthEndpoint().getProfile(new UserViewProfileRequest(getToken())).getUser();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("ROLE: " + user.getRole());
    }

}