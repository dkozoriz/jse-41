package ru.t1.dkozoriz.tm.api.service;

public interface IServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    IAuthService getAuthService();

    IPropertyService getPropertyService();

    ISessionService getSessionService();

    IConnectionService getConnectionService();

}