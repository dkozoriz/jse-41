package ru.t1.dkozoriz.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.dto.request.project.*;
import ru.t1.dkozoriz.tm.dto.response.project.*;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.dto.model.SessionDTO;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.dkozoriz.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ShowProjectListResponse projectList(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowListRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<ProjectDTO> projectList = getServiceLocator().getProjectService().findAll(userId, sort);
        return new ShowProjectListResponse(projectList);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ChangeProjectStatusByIdResponse projectChangeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO project =
                getServiceLocator().getProjectService().changeStatusById(userId, projectId, status);
        return new ChangeProjectStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ChangeProjectStatusByIndexResponse projectChangeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIndexRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDTO project =
                getServiceLocator().getProjectService().changeStatusByIndex(userId, projectIndex, status);
        return new ChangeProjectStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public CompleteProjectByIdResponse projectCompleteById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final ProjectDTO project =
                getServiceLocator().getProjectService().changeStatusById(userId, projectId, Status.COMPLETED);
        return new CompleteProjectByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public CompleteProjectByIndexResponse projectCompleteByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIndexRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final ProjectDTO project =
                getServiceLocator().getProjectService().changeStatusByIndex(userId, projectIndex, Status.COMPLETED);
        return new CompleteProjectByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public StartProjectByIdResponse projectStartById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final ProjectDTO project =
                getServiceLocator().getProjectService().changeStatusById(userId, projectId, Status.IN_PROGRESS);
        return new StartProjectByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public StartProjectByIndexResponse projectStartByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIndexRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final ProjectDTO project =
                getServiceLocator().getProjectService().changeStatusByIndex(userId, projectIndex, Status.IN_PROGRESS);
        return new StartProjectByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public CreateProjectResponse projectCreate(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project =
                getServiceLocator().getProjectService().create(userId, name, description);
        return new CreateProjectResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ListClearProjectResponse projectListClear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListClearRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        getServiceLocator().getProjectService().clear(userId);
        return new ListClearProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public RemoveProjectByIdResponse projectRemoveById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        getServiceLocator().getProjectService().removeById(userId, projectId);
        return new RemoveProjectByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public RemoveProjectByIndexResponse projectRemoveByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIndexRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        getServiceLocator().getProjectService().removeByIndex(userId, projectIndex);
        return new RemoveProjectByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ShowProjectByIdResponse projectShowById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final ProjectDTO project =
                getServiceLocator().getProjectService().findById(userId, projectId);
        return new ShowProjectByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ShowProjectByIndexResponse projectShowByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIndexRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final ProjectDTO project =
                getServiceLocator().getProjectService().findByIndex(userId, projectIndex);
        return new ShowProjectByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UpdateProjectByIdResponse projectUpdateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project =
                getServiceLocator().getProjectService().updateById(userId, projectId, name, description);
        return new UpdateProjectByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UpdateProjectByIndexResponse projectUpdateByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIndexRequest request
    ) {
        @NotNull final SessionDTO session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDTO project =
                getServiceLocator().getProjectService().updateByIndex(userId, projectIndex, name, description);
        return new UpdateProjectByIndexResponse(project);
    }

}