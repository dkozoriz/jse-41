package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.model.SessionDTO;
import ru.t1.dkozoriz.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String Email) throws Exception;

    String login(@Nullable String login, @Nullable String password) throws Exception;

    @NotNull
    SessionDTO validateToken(@Nullable String token);

    void invalidate(@Nullable SessionDTO session) throws Exception;

}