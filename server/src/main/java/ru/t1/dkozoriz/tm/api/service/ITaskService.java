package ru.t1.dkozoriz.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDTO;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable String projectId
    ) ;

    int getSize(@Nullable String userId);

    List<TaskDTO> findAll(@Nullable String userId);

    List<TaskDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator comparator);

    TaskDTO findById(@Nullable String userId, @Nullable String id);

    TaskDTO findByIndex(@Nullable String userId, @Nullable Integer index);

    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    public TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    TaskDTO changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    TaskDTO changeStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    void clear(@Nullable String userId);

    @NotNull
    TaskDTO bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    TaskDTO unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}