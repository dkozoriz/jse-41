package ru.t1.dkozoriz.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IProjectRepository;
import ru.t1.dkozoriz.tm.api.repository.ISessionRepository;
import ru.t1.dkozoriz.tm.api.repository.ITaskRepository;
import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.IUserService;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.EmailEmptyException;
import ru.t1.dkozoriz.tm.exception.field.EmailIsExistException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.RoleEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginIsExistException;
import ru.t1.dkozoriz.tm.exception.user.PasswordEmptyException;
import ru.t1.dkozoriz.tm.dto.model.UserDTO;
import ru.t1.dkozoriz.tm.util.HashUtil;

import java.util.Collections;
import java.util.List;


public final class UserService implements IUserService {

    private final static String NAME = "User";

    @NotNull
    private final IServiceLocator serviceLocator;

    public UserService(@NotNull IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public String getName() {
        return NAME;
    }

    @NotNull
    private IConnectionService getConnectionService() {
        return serviceLocator.getConnectionService();
    }

    private void add(@NotNull final UserDTO user) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            userRepository.add(user);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    private void update(@NotNull final UserDTO user) {
        @NotNull final SqlSession session = getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            userRepository.update(user);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @NotNull
    @Override
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        add(user);
        return user;
    }

    @NotNull
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new EmailIsExistException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        add(user);
        return user;
    }

    @NotNull
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            @Nullable final List<UserDTO> users = userRepository.findAll();
            if (users == null) return Collections.emptyList();
            return users;
        }
    }

    @Nullable
    public UserDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            return userRepository.findById(id);
        }
    }

    @Nullable
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            return userRepository.findByLogin(login);
        }
    }

    @Nullable
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            return userRepository.findByEmail(email);
        }
    }

    public void remove(@Nullable final UserDTO user) {
        if (user == null) throw new EntityException(getName());
        @Nullable final String userId = user.getId();
        @NotNull final SqlSession session = getConnectionService().getSqlSession();
        try {
            @NotNull final ITaskRepository taskRepository = session.getMapper(ITaskRepository.class);
            @NotNull final IProjectRepository projectRepository = session.getMapper(IProjectRepository.class);
            @NotNull final ISessionRepository sessionRepository = session.getMapper(ISessionRepository.class);
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            taskRepository.clearWithUserId(userId);
            projectRepository.clearWithUserId(userId);
            sessionRepository.clearWithUserId(userId);
            userRepository.remove(user);
            session.commit();
        } catch (@NotNull final Exception e) {
            session.rollback();
            throw e;
        } finally {
            session.close();
        }
    }


    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findById(id);
        if (user == null) throw new EntityException(getName());
        remove(user);
    }

    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        remove(user);
    }

    public void removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new EntityException(getName());
        remove(user);
    }

    public int getSize() {
        try (@NotNull final SqlSession session = getConnectionService().getSqlSession()) {
            @NotNull final IUserRepository userRepository = session.getMapper(IUserRepository.class);
            return userRepository.getSize();
        }
    }

    @NotNull
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findById(id);
        if (user == null) throw new EntityException(getName());
        @Nullable final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        update(user);
        return user;
    }

    @NotNull
    public UserDTO updateUser(@Nullable final String id,
                              @Nullable final String firstName,
                              @Nullable final String lastName,
                              @Nullable final String middleName
    ) {

        @Nullable final UserDTO user = findById(id);
        if (user == null) throw new EntityException(getName());
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @NotNull
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        user.setLocked(true);
        update(user);
    }

    public void unLockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        user.setLocked(false);
        update(user);
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = getConnectionService().getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            userRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}