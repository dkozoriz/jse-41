package ru.t1.dkozoriz.tm.dto.response.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.response.AbstractResponse;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UpdateTaskByIndexResponse extends AbstractResponse {

    @Nullable
    private TaskDTO task;

}