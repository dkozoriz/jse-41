package ru.t1.dkozoriz.tm.api.model;

public interface ICommand {

    void execute();

}
