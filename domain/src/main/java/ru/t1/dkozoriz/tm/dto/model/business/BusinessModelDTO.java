package ru.t1.dkozoriz.tm.dto.model.business;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.dto.model.UserOwnedModelDTO;
import ru.t1.dkozoriz.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;


@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class BusinessModelDTO extends UserOwnedModelDTO implements IWBS {

    @NotNull
    @Column(nullable = false, name = "name")
    protected String name = "";

    @Nullable
    @Column(name = "description")
    protected String description = "";

    @NotNull
    @Column(nullable = false, name = "status")
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "date")
    protected Date created = new Date();

    public BusinessModelDTO(@NotNull final String name) {
        this.name = name;
    }

    public BusinessModelDTO(@NotNull final String name, @NotNull final Status status) {
        this.name = name;
        this.status = status;
    }

}