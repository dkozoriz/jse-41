package ru.t1.dkozoriz.tm.dto.request.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UserLockRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    public UserLockRequest(
            @Nullable final String token,
            @Nullable final String login
    ) {
        super(token);
        this.login = login;
    }

}