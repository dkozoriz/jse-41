package ru.t1.dkozoriz.tm.dto.model.business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.model.IWBS;
import ru.t1.dkozoriz.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_task")
public final class TaskDTO extends BusinessModelDTO implements IWBS {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = "project_id")
    private String projectId;

    public TaskDTO(@NotNull final String name) {
        super(name);
    }

    public TaskDTO(@NotNull final String name, @NotNull final Status status) {
        super(name, status);
    }

}