package ru.t1.dkozoriz.tm.dto.response.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class UnbindTaskToProjectResponse extends AbstractUserRequest {

    @Nullable
    private TaskDTO task;

}